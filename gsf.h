#pragma once
extern void DisplayError(char *, ...);

extern bool IsTagPresent(uint8_t *);
extern bool IsValidGSF(uint8_t *);
extern void setupSound(void);
extern int GSFRun(char *);
extern void GSFClose(void);
extern bool EmulationLoop(void);
