#include <fcntl.h>
#include <libgen.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <stdint.h>
#include <SDL.h>

#include "config.h"

extern "C" {
#include "VBA/psftag.h"
#include "gsf.h"
}

extern "C" {
int defvolume = 1000;
int relvolume = 1000;
int TrackLength = 0;
int FadeLength = 0;
int IgnoreTrackLength, DefaultLength = 150000;
int playforever = 0;
int TrailingSilence = 1000;
int DetectSilence = 0, silencedetected = 0, silencelength = 5;
}
int cpupercent = 0, sndSamplesPerSec, sndNumChannels;
int sndBitsPerSample = 16;

int deflen = 120, deffade = 4;

extern uint16_t soundFinalWave[];
extern int soundBufferLen;

extern char soundEcho;
extern char soundLowPass;
extern char soundReverse;
extern char soundQuality;

static size_t audio_sent;

double decode_pos_ms; // current decoding position, in milliseconds
int seek_needed; // if != -1, it is the point that the decode thread should seek
                 // to, in ms.

static int g_playing = 0;
static int g_must_exit = 0;

static SDL_AudioDeviceID snd_dev;

extern "C" int LengthFromString(const char *timestring);
extern "C" int VolumeFromString(const char *volumestring);

extern "C" void end_of_track() { g_playing = 0; }

extern "C" void writeSound(void) {
	size_t tmp = SDL_GetQueuedAudioSize(snd_dev);
	int nframe = tmp / (2 * sndNumChannels);
	float sec = ((float)nframe)/((float)sndSamplesPerSec);
	if (sec > 0.1) {
		//More than 0.1 sec of audio is queued,
		//Sleep for a while
		SDL_Delay((int)(sec*1000)-100);
		tmp = SDL_GetQueuedAudioSize(snd_dev);
	}

	int delta = audio_sent-tmp;
	decode_pos_ms +=
	    (delta / (2 * sndNumChannels) * 1000) / (float)sndSamplesPerSec;
	int ret = SDL_QueueAudio(snd_dev, soundFinalWave, soundBufferLen);
	audio_sent = tmp+soundBufferLen;
	if (ret < 0)
		fprintf(stderr, "Failed to queue audio: %s", SDL_GetError());
}

extern "C" void signal_handler(int sig) {
	struct timeval tv_now;
	int elaps_milli;

	static int first = 1;
	static struct timeval last_int = {0, 0};

	g_playing = 0;
	gettimeofday(&tv_now, NULL);

	if (first) {
		first = 0;
	} else {
		elaps_milli = (tv_now.tv_sec - last_int.tv_sec) * 1000;
		elaps_milli += (tv_now.tv_usec - last_int.tv_usec) / 1000;

		if (elaps_milli < 1500) {
			g_must_exit = 1;
		}
	}
	memcpy(&last_int, &tv_now, sizeof(struct timeval));
}

static void shuffle_list(char *filelist[], int num_files) {
	int i, n;
	char *tmp;
	srand((int)time(NULL));
	for (i = 0; i < num_files; i++) {
		tmp = filelist[i];
		n = (int)((double)num_files * rand() / (RAND_MAX + 1.0));
		filelist[i] = filelist[n];
		filelist[n] = tmp;
	}
}

#define BOLD() printf("%c[36m", 27);
#define NORMAL() printf("%c[0m", 27);

int main(int argc, char **argv) {
	SDL_Init(SDL_INIT_AUDIO);
	int r, tmp, fi, random = 0;
	char Buffer[1024];
	char length_str[256], fade_str[256], volume[256], title_str[256];
	char tmp_str[256];
	char *tag;

	soundLowPass = 0;
	soundEcho = 0;
	soundQuality = 0;

	DetectSilence = 1;
	silencelength = 5;
	IgnoreTrackLength = 0;
	DefaultLength = 150000;
	TrailingSilence = 1000;
	playforever = 0;

	while ((r = getopt(argc, argv, "hlsrieL:t:")) >= 0) {
		char *e;
		switch (r) {
		case 'h':
			printf("playgsf version %s (based on Highly Advanced)\n\n",
			       VERSION_STR);
			printf("Usage: ./playgsf [options] files...\n\n");
			printf("  -l        Enable low pass filer\n");
			printf("  -s        Detect silence\n");
			printf("  -L        Set silence length in seconds (for "
			       "detection). Default 5\n");
			printf("  -t        Set default track length in "
			       "milliseconds. Default 150000 ms\n");
			printf("  -i        Ignore track length (use default "
			       "length)\n");
			printf("  -e        Endless play\n");
			printf("  -r        Play files in random order\n");
			printf("  -h        Displays what you are reading "
			       "right now\n");
			return 0;
			break;
		case 'i':
			IgnoreTrackLength = 1;
			break;
		case 'l':
			soundLowPass = 1;
			break;
		case 's':
			DetectSilence = 1;
			break;
		case 'L':
			silencelength = strtol(optarg, &e, 0);
			if (e == optarg) {
				fprintf(stderr, "Bad value\n");
				return 1;
			}
			break;
		case 'e':
			playforever = 1;
			break;
		case 't':
			DefaultLength = strtol(optarg, &e, 0);
			if (e == optarg) {
				fprintf(stderr, "Bad value\n");
				return 1;
			}
			break;
		case 'r':
			random = 1;
			break;
		case '?':
			fprintf(stderr, "Unknown argument. try -h\n");
			return 1;
			break;
		}
	}

	if (argc - optind <= 0) {
		printf("No files specified! For help, try -h\n");
		return 1;
	}

	if (random) {
		shuffle_list(&argv[optind], argc - optind);
	}

	printf("playgsf version %s (based on Highly Advanced)\n\n",
	       VERSION_STR);

	signal(SIGINT, signal_handler);

	tag = (char *)malloc(50001);

	fi = optind;
	while (!g_must_exit && fi < argc) {
		decode_pos_ms = 0;
		seek_needed = -1;
		TrailingSilence = 1000;

		r = GSFRun(argv[fi]);
		if (!r) {
			fi++;
			continue;
		}

		g_playing = 1;

		psftag_readfromfile((void *)tag, argv[fi]);

		BOLD();
		printf("Filename: ");
		NORMAL();
		printf("%s\n", basename(argv[fi]));
		BOLD();
		printf("Channels: ");
		NORMAL();
		printf("%d\n", sndNumChannels);
		BOLD();
		printf("Sample rate: ");
		NORMAL();
		printf("%d\n", sndSamplesPerSec);

		if (!psftag_getvar(tag, "title", title_str,
		                   sizeof(title_str) - 1)) {
			BOLD();
			printf("Title: ");
			NORMAL();
			printf("%s\n", title_str);
		}

		if (!psftag_getvar(tag, "artist", tmp_str,
		                   sizeof(tmp_str) - 1)) {
			BOLD();
			printf("Artist: ");
			NORMAL();
			printf("%s\n", tmp_str);
		}

		if (!psftag_getvar(tag, "game", tmp_str, sizeof(tmp_str) - 1)) {
			BOLD();
			printf("Game: ");
			NORMAL();
			printf("%s\n", tmp_str);
		}

		if (!psftag_getvar(tag, "year", tmp_str, sizeof(tmp_str) - 1)) {
			BOLD();
			printf("Year: ");
			NORMAL();
			printf("%s\n", tmp_str);
		}

		if (!psftag_getvar(tag, "copyright", tmp_str,
		                   sizeof(tmp_str) - 1)) {
			BOLD();
			printf("Copyright: ");
			NORMAL();
			printf("%s\n", tmp_str);
		}

		if (!psftag_getvar(tag, "gsfby", tmp_str,
		                   sizeof(tmp_str) - 1)) {
			BOLD();
			printf("GSF By: ");
			NORMAL();
			printf("%s\n", tmp_str);
		}

		if (!psftag_getvar(tag, "tagger", tmp_str,
		                   sizeof(tmp_str) - 1)) {
			BOLD();
			printf("Tagger: ");
			NORMAL();
			printf("%s\n", tmp_str);
		}

		if (!psftag_getvar(tag, "comment", tmp_str,
		                   sizeof(tmp_str) - 1)) {
			BOLD();
			printf("Comment: ");
			NORMAL();
			printf("%s\n", tmp_str);
		}

		if (!psftag_getvar(tag, "fade", fade_str,
		                   sizeof(fade_str) - 1)) {
			FadeLength = LengthFromString(fade_str);
			BOLD();
			printf("Fade: ");
			NORMAL();
			printf("%s (%d ms)\n", fade_str, FadeLength);
		}

		if (!psftag_raw_getvar(tag, "length", length_str,
		                       sizeof(length_str) - 1)) {
			TrackLength = LengthFromString(length_str) + FadeLength;
			BOLD();
			printf("Length: ");
			NORMAL();
			printf("%s (%d ms) ", length_str, TrackLength);
			if (IgnoreTrackLength) {
				printf("(ignored)");
				TrackLength = DefaultLength;
			}
			printf("\n");
		} else {
			TrackLength = DefaultLength;
		}

		/* Must be done after GSFrun so sndNumchannels and
		 * sndSamplesPerSec are set to valid values */

		SDL_AudioSpec want, have;

		memset(&want, 0, sizeof(want));
		want.freq = sndSamplesPerSec;
		want.format = AUDIO_S16LSB;
		want.channels = 2;
		want.samples = 4096;
		snd_dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, 0);
		if (snd_dev == 0) {
			fprintf(stderr, "Failed to open audio: %s\n", SDL_GetError());
			return -1;
		}
		if (want.format != have.format)
			fprintf(stderr, "Get different audio format, enjoy some noise!\n");
		SDL_PauseAudioDevice(snd_dev, 0);

		while (g_playing) {
			int remaining = TrackLength - (int)decode_pos_ms;
			if (remaining < 0) {
				// this happens during silence period
				remaining = 0;
			}
			EmulationLoop();

			BOLD();
			printf("Time: ");
			NORMAL();
			printf("%02d:%02d.%02d ",
			       (int)(decode_pos_ms / 1000.0) / 60,
			       (int)(decode_pos_ms / 1000.0) % 60,
			       (int)(decode_pos_ms / 10.0) % 100);
			if (!playforever) {
				/*BOLD();*/ printf("["); /*NORMAL();*/
				printf("%02d:%02d.%02d", remaining / 1000 / 60,
				       (remaining / 1000) % 60,
				       (remaining / 10 % 100));
				/*BOLD();*/ printf("] of "); /*NORMAL();*/
				printf("%02d:%02d.%02d ",
				       TrackLength / 1000 / 60,
				       (TrackLength / 1000) % 60,
				       (TrackLength / 10 % 100));
			}
			BOLD();
			printf("  GBA Cpu: ");
			NORMAL();
			printf("%02d%% ", cpupercent);
			printf("     \r");

			fflush(stdout);
		}
		printf("\n--\n");
		SDL_CloseAudioDevice(snd_dev);
		fi++;
	}

	return 0;
}
