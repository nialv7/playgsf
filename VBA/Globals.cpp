// VisualBoyAdvance - Nintendo Gameboy/GameboyAdvance (TM) emulator.
// Copyright (C) 1999-2003 Forgotten
// Copyright (C) 2004 Forgotten and the VBA development team

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or(at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "GBA.h"

reg_pair reg[45];
memoryMap map[256];
bool ioReadable[0x400];
bool N_FLAG = 0;
bool C_FLAG = 0;
bool Z_FLAG = 0;
bool V_FLAG = 0;
bool armState = true;
bool armIrqEnable = true;
uint32_t armNextPC = 0x00000000;
int armMode = 0x1f;
uint32_t stop = 0x08000568;
int saveType = 0;
bool useBios = false;
bool skipBios = false;
int frameSkip = 1;
bool speedup = false;
bool synchronize = true;
bool cpuDisableSfx = false;
bool cpuIsMultiBoot = false;
bool parseDebug = true;
int layerSettings = 0xff00;
int layerEnable = 0xff00;
bool speedHack = false;
int cpuSaveType = 0;
bool cpuEnhancedDetection = true;
bool cheatsEnabled = true;

uint8_t *bios = NULL;
uint8_t *rom = NULL;
uint8_t *internalRAM = NULL;
uint8_t *workRAM = NULL;
uint8_t *paletteRAM = NULL;
uint8_t *vram = NULL;
// uint8_t *pix = NULL;
uint8_t *oam = NULL;
uint8_t *ioMem = NULL;

uint16_t DISPCNT = 0x0080;
uint16_t DISPSTAT = 0x0000;
uint16_t VCOUNT = 0x0000;
uint16_t BG0CNT = 0x0000;
uint16_t BG1CNT = 0x0000;
uint16_t BG2CNT = 0x0000;
uint16_t BG3CNT = 0x0000;
uint16_t BG0HOFS = 0x0000;
uint16_t BG0VOFS = 0x0000;
uint16_t BG1HOFS = 0x0000;
uint16_t BG1VOFS = 0x0000;
uint16_t BG2HOFS = 0x0000;
uint16_t BG2VOFS = 0x0000;
uint16_t BG3HOFS = 0x0000;
uint16_t BG3VOFS = 0x0000;
uint16_t BG2PA = 0x0100;
uint16_t BG2PB = 0x0000;
uint16_t BG2PC = 0x0000;
uint16_t BG2PD = 0x0100;
uint16_t BG2X_L = 0x0000;
uint16_t BG2X_H = 0x0000;
uint16_t BG2Y_L = 0x0000;
uint16_t BG2Y_H = 0x0000;
uint16_t BG3PA = 0x0100;
uint16_t BG3PB = 0x0000;
uint16_t BG3PC = 0x0000;
uint16_t BG3PD = 0x0100;
uint16_t BG3X_L = 0x0000;
uint16_t BG3X_H = 0x0000;
uint16_t BG3Y_L = 0x0000;
uint16_t BG3Y_H = 0x0000;
uint16_t WIN0H = 0x0000;
uint16_t WIN1H = 0x0000;
uint16_t WIN0V = 0x0000;
uint16_t WIN1V = 0x0000;
uint16_t WININ = 0x0000;
uint16_t WINOUT = 0x0000;
uint16_t MOSAIC = 0x0000;
uint16_t BLDMOD = 0x0000;
uint16_t COLEV = 0x0000;
uint16_t COLY = 0x0000;
uint16_t DM0SAD_L = 0x0000;
uint16_t DM0SAD_H = 0x0000;
uint16_t DM0DAD_L = 0x0000;
uint16_t DM0DAD_H = 0x0000;
uint16_t DM0CNT_L = 0x0000;
uint16_t DM0CNT_H = 0x0000;
uint16_t DM1SAD_L = 0x0000;
uint16_t DM1SAD_H = 0x0000;
uint16_t DM1DAD_L = 0x0000;
uint16_t DM1DAD_H = 0x0000;
uint16_t DM1CNT_L = 0x0000;
uint16_t DM1CNT_H = 0x0000;
uint16_t DM2SAD_L = 0x0000;
uint16_t DM2SAD_H = 0x0000;
uint16_t DM2DAD_L = 0x0000;
uint16_t DM2DAD_H = 0x0000;
uint16_t DM2CNT_L = 0x0000;
uint16_t DM2CNT_H = 0x0000;
uint16_t DM3SAD_L = 0x0000;
uint16_t DM3SAD_H = 0x0000;
uint16_t DM3DAD_L = 0x0000;
uint16_t DM3DAD_H = 0x0000;
uint16_t DM3CNT_L = 0x0000;
uint16_t DM3CNT_H = 0x0000;
uint16_t TM0D = 0x0000;
uint16_t TM0CNT = 0x0000;
uint16_t TM1D = 0x0000;
uint16_t TM1CNT = 0x0000;
uint16_t TM2D = 0x0000;
uint16_t TM2CNT = 0x0000;
uint16_t TM3D = 0x0000;
uint16_t TM3CNT = 0x0000;
uint16_t P1 = 0xFFFF;
uint16_t IE = 0x0000;
uint16_t IF = 0x0000;
uint16_t IME = 0x0000;
