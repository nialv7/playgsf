// -*- C++ -*-
// VisualBoyAdvance - Nintendo Gameboy/GameboyAdvance (TM) emulator.
// Copyright (C) 1999-2003 Forgotten
// Copyright (C) 2004 Forgotten and the VBA development team

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or(at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef VBA_GLOBALS_H
#define VBA_GLOBALS_H

#define VERBOSE_SWI 1
#define VERBOSE_UNALIGNED_MEMORY 2
#define VERBOSE_ILLEGAL_WRITE 4
#define VERBOSE_ILLEGAL_READ 8
#define VERBOSE_DMA0 16
#define VERBOSE_DMA1 32
#define VERBOSE_DMA2 64
#define VERBOSE_DMA3 128
#define VERBOSE_UNDEFINED 256
#define VERBOSE_AGBPRINT 512

extern reg_pair reg[45];
extern bool ioReadable[0x400];
extern bool N_FLAG;
extern bool C_FLAG;
extern bool Z_FLAG;
extern bool V_FLAG;
extern bool armState;
extern bool armIrqEnable;
extern uint32_t armNextPC;
extern int armMode;
extern uint32_t stop;
extern int saveType;
extern bool useBios;
extern bool skipBios;
extern int frameSkip;
extern bool speedup;
extern bool synchronize;
extern bool cpuDisableSfx;
extern bool cpuIsMultiBoot;
extern bool parseDebug;
extern int layerSettings;
extern int layerEnable;
extern bool speedHack;
extern int cpuSaveType;
extern bool cpuEnhancedDetection;
extern bool cheatsEnabled;

extern uint8_t *bios;
extern uint8_t *rom;
extern uint8_t *internalRAM;
extern uint8_t *workRAM;
extern uint8_t *paletteRAM;
extern uint8_t *vram;
// extern uint8_t *pix;
extern uint8_t *oam;
extern uint8_t *ioMem;

extern uint16_t DISPCNT;
extern uint16_t DISPSTAT;
extern uint16_t VCOUNT;
extern uint16_t BG0CNT;
extern uint16_t BG1CNT;
extern uint16_t BG2CNT;
extern uint16_t BG3CNT;
extern uint16_t BG0HOFS;
extern uint16_t BG0VOFS;
extern uint16_t BG1HOFS;
extern uint16_t BG1VOFS;
extern uint16_t BG2HOFS;
extern uint16_t BG2VOFS;
extern uint16_t BG3HOFS;
extern uint16_t BG3VOFS;
extern uint16_t BG2PA;
extern uint16_t BG2PB;
extern uint16_t BG2PC;
extern uint16_t BG2PD;
extern uint16_t BG2X_L;
extern uint16_t BG2X_H;
extern uint16_t BG2Y_L;
extern uint16_t BG2Y_H;
extern uint16_t BG3PA;
extern uint16_t BG3PB;
extern uint16_t BG3PC;
extern uint16_t BG3PD;
extern uint16_t BG3X_L;
extern uint16_t BG3X_H;
extern uint16_t BG3Y_L;
extern uint16_t BG3Y_H;
extern uint16_t WIN0H;
extern uint16_t WIN1H;
extern uint16_t WIN0V;
extern uint16_t WIN1V;
extern uint16_t WININ;
extern uint16_t WINOUT;
extern uint16_t MOSAIC;
extern uint16_t BLDMOD;
extern uint16_t COLEV;
extern uint16_t COLY;
extern uint16_t DM0SAD_L;
extern uint16_t DM0SAD_H;
extern uint16_t DM0DAD_L;
extern uint16_t DM0DAD_H;
extern uint16_t DM0CNT_L;
extern uint16_t DM0CNT_H;
extern uint16_t DM1SAD_L;
extern uint16_t DM1SAD_H;
extern uint16_t DM1DAD_L;
extern uint16_t DM1DAD_H;
extern uint16_t DM1CNT_L;
extern uint16_t DM1CNT_H;
extern uint16_t DM2SAD_L;
extern uint16_t DM2SAD_H;
extern uint16_t DM2DAD_L;
extern uint16_t DM2DAD_H;
extern uint16_t DM2CNT_L;
extern uint16_t DM2CNT_H;
extern uint16_t DM3SAD_L;
extern uint16_t DM3SAD_H;
extern uint16_t DM3DAD_L;
extern uint16_t DM3DAD_H;
extern uint16_t DM3CNT_L;
extern uint16_t DM3CNT_H;
extern uint16_t TM0D;
extern uint16_t TM0CNT;
extern uint16_t TM1D;
extern uint16_t TM1CNT;
extern uint16_t TM2D;
extern uint16_t TM2CNT;
extern uint16_t TM3D;
extern uint16_t TM3CNT;
extern uint16_t P1;
extern uint16_t IE;
extern uint16_t IF;
extern uint16_t IME;

#endif // VBA_GLOBALS_H
