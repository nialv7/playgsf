// -*- C++ -*-
// VisualBoyAdvance - Nintendo Gameboy/GameboyAdvance (TM) emulator.
// Copyright (C) 1999-2003 Forgotten
// Copyright (C) 2004 Forgotten and the VBA development team

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or(at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef VBA_UTIL_H
#define VBA_UTIL_H
enum IMAGE_TYPE { IMAGE_UNKNOWN = -1, IMAGE_GBA = 0, IMAGE_GB = 1 };

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct {
	void *address;
	int size;
} variable_desc;

typedef struct vbaFileOps vbaFile;

struct vbaFileOps {
	int (*write)(vbaFile *, const void*, size_t);
	int (*read)(vbaFile *, void *, size_t);
	int (*close)(vbaFile *);
};

// extern bool utilWritePNGFile(const char *, int, int, uint8_t *);
// extern bool utilWriteBMPFile(const char *, int, int, uint8_t *);
// extern void utilApplyIPS(const char *ips, uint8_t **rom, int *size);
// extern void utilWriteBMP(char *, int, int, uint8_t *);
extern bool utilIsGBAImage(const char *);
// extern bool utilIsGBImage(const char *);
// extern bool utilIsZipFile(const char *);
// extern bool utilIsGzipFile(const char *);
// extern bool utilIsRarFile(const char *);
extern void utilGetBaseName(const char *, char *);
extern void utilGetBasePath(const char *file, char *buffer);
extern IMAGE_TYPE utilFindType(const char *);
extern uint8_t *utilLoad(const char *, bool (*)(const char *), uint8_t *, size_t &);

extern void utilPutDword(uint8_t *, uint32_t);
extern void utilPutWord(uint8_t *, uint16_t);
extern void utilWriteData(vbaFile *, variable_desc *);
extern void utilReadData(vbaFile *, variable_desc *);
extern int utilReadInt(vbaFile *);
extern void utilWriteInt(vbaFile *, int);
extern vbaFile *utilGzOpen(const char *file, const char *mode);
extern vbaFile *utilMemGzOpen(char *memory, int available, char *mode);
extern int utilGzWrite(vbaFile *file, const void *buffer, size_t len);
extern int utilGzRead(vbaFile *file, void *buffer, size_t len);
extern int utilGzClose(vbaFile *file);
extern void utilGBAFindSave(const uint8_t *, const int);
#endif
