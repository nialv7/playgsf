/* gzio.c -- IO on .gz files
 * Copyright (C) 1995-2002 Jean-loup Gailly.
 * For conditions of distribution and use, see copyright notice in zlib.h
 *
 * Compile this file with -DNO_DEFLATE to avoid the compression code.
 */

/* memgzio.c - IO on .gz files in memory
 * Adapted from original gzio.c from zlib library by Forgotten
 */
#ifndef HAVE_ZUTIL_H
#include "../zlib/zutil.h"
#else
#include <zutil.h>
#endif

#include <stdlib.h>
#include "Util.h"

struct mem_stream;

typedef struct mem_stream mem_stream;

vbaFile *memgzopen(char *memory, int, const char *);
int memgzread(vbaFile*, void *, size_t);
int memgzwrite(vbaFile *, const void *, size_t);
int memgzclose(vbaFile *);
